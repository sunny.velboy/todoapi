﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EFDataAccessLibrary.DataAccess;
using EFDataAccessLibrary.Models;
using Microsoft.EntityFrameworkCore;


namespace EFDataAccessLibrary.DataAccess
{
   

    public class Context : DbContext
    {
       
        public Context(DbContextOptions<Context> options) : base(options) { }
      
        public DbSet<ToDoTask> Tasks { get; set; }
      
        public DbSet<Tag> Tags { get; set; }
       
        public DbSet<Priority> Priorities { get; set; }
       
        public DbSet<Status> Statuses { get; set; }

       
    }
}
