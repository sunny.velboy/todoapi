﻿using System;
using System.Collections.Generic;

namespace EFDataAccessLibrary.Models
{
    public class Priority
    {
        public PriorityList Id { get; set; } 
        public string Name { get; set; }

        public ICollection<ToDoTask> Tasks { get; set; }

        public Priority()
        {
            Tasks = new List<ToDoTask>();
        }

    }
}
