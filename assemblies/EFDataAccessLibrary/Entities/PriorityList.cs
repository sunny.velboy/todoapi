﻿using System;
namespace EFDataAccessLibrary.Models
{
    public enum PriorityList
    {
        VeryImportant = 1,
        Important = 2,
        ToDo = 3,
        ToDoLater = 4,
    }
}
