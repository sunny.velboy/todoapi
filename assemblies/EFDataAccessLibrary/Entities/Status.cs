﻿using System;
using System.Collections.Generic;

namespace EFDataAccessLibrary.Models
{
    public class Status
    {
        public StatusesList Id { get; set; }
        public string Name { get; set; }

        public ICollection<ToDoTask> Tasks { get; set; } = new List<ToDoTask>();

        public Status()
        {
            Tasks = new List<ToDoTask>();
        }

    }
}
