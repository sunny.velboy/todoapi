﻿using System;
namespace EFDataAccessLibrary.Models
{
    public enum StatusesList
    {
        Done = 1,
        NotDone = 2
    }
}
