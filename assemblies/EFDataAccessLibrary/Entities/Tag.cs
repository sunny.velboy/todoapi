﻿using System;
using System.Collections.Generic;

namespace EFDataAccessLibrary.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
       
        public ICollection<ToDoTask> Tasks { get; set; }
        public Tag()
        {
            Tasks = new List<ToDoTask>();
        }

    }
}
