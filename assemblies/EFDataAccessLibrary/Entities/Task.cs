﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFDataAccessLibrary.Models
{
    
    public class ToDoTask
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public int? PriorityId { get; set; }
        public Priority Priority { get; set; }

        public int? StatusID { get; set; }
        public Status Statuses { get; set; }

        public ICollection<Tag> Tags { get; set; }
        public ToDoTask()
        {
            Tags = new List<Tag>();
        }

    }
}
