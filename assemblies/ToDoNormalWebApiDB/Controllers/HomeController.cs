﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFDataAccessLibrary.DataAccess;
using EFDataAccessLibrary.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Pomelo.EntityFrameworkCore;


namespace ToDoNormalWebApiDB.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HomeController : Controller
    {
        private readonly IRepository<ToDoTask> _repository;

        public HomeController(IRepository<ToDoTask> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ToDoTask>> Get()
        {
            object result = _repository.GetTaskList(); // переименовтаь а айэкшн
            return Ok(result);
            
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id) // почитать про роутинг еще раз
        {
           ToDoTask task = _repository.GetTask(id).Result;
            if (task == null)
                return NotFound();
            return StatusCode(StatusCodes.Status200OK, task);
        }

        [HttpPost]
        public IActionResult Create(ToDoTask task)
        {
            _repository.Create(task);
            _repository.Save();
            return StatusCode(StatusCodes.Status201Created, task);
        }

        [HttpPut]
        public IActionResult Update(ToDoTask task)
        {
            _repository.Update(task);
            _repository.Save();
            return StatusCode(StatusCodes.Status200OK, task); // изменено
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            ToDoTask task = _repository.GetTask(id).Result;
            if (task == null)
            {
                return NotFound(); // тоже протестировать
            }
            // 
            _repository.Delete(id);
            _repository.Save();
            return StatusCode(StatusCodes.Status200OK); // изменено 
        }


    }
}

