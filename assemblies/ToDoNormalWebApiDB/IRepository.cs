﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ToDoNormalWebApiDB
{
    public interface IRepository<T> : IDisposable
         where T : class
    {
        Task<IEnumerable<T>> GetTaskList(); // получение всех объектов
        Task<T> GetTask(int id); // получение одного объекта по id
        void Create(T item); // создание объекта
        void Update(T item); // обновление объекта
        void Delete(int id); // удаление объекта по id
        void Save();  // сохранение изменений
        
    }
}
