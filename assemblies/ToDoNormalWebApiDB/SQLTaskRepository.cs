﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EFDataAccessLibrary.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace ToDoNormalWebApiDB
{
    
   
        public class SQLTaskRepository : IRepository<EFDataAccessLibrary.Models.ToDoTask>
        {
            private Context db;

            public SQLTaskRepository(Context context)
            {
                db = context;
            }

            public async Task<IEnumerable<EFDataAccessLibrary.Models.ToDoTask>> GetTaskList() // как это проверять?
            {
                return await db.Tasks.ToListAsync();
            }

            public async Task<EFDataAccessLibrary.Models.ToDoTask> GetTask(int id)
            {
                return await db.Tasks.FindAsync(id);
            }

            public async void Create(EFDataAccessLibrary.Models.ToDoTask task)
            {
                await db.Tasks.AddAsync(task);
            }

            public async void Update(EFDataAccessLibrary.Models.ToDoTask task)
            {
                db.Entry(task).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }

            public async void Delete(int id)
            {
               var task = await db.Tasks.FindAsync(id);
                if (task != null)
                    db.Tasks.Remove(task);
            }

            public async void Save()
            {
                await db.SaveChangesAsync();
            }

            private bool disposed = false;

            public virtual void Dispose(bool disposing)
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        db.Dispose();
                    }
                }
                this.disposed = true;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
    
}
