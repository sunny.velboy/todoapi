﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFDataAccessLibrary.DataAccess;
using EFDataAccessLibrary.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Pomelo.EntityFrameworkCore;

namespace ToDoNormalWebApiDB
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            MySqlServerVersion serverVersion = new MySqlServerVersion(new Version(8, 0, 27));

            string con = "Server=localhost;Database=TasksDB;Uid=root;Pwd=zxc123QWE;";


            //services.AddDbContext<Context>(
            //dbContextOptions => dbContextOptions
            //    .UseMySql(con, serverVersion)
            //    .LogTo(Console.WriteLine, LogLevel.Information)
            //    .EnableSensitiveDataLogging()
            //    .EnableDetailedErrors()
            //);

            
            services.AddDbContext<Context>(options => options.UseMySql(con, serverVersion));

            services.AddScoped<IRepository<ToDoTask>, SQLTaskRepository>();

            services.AddControllers();
        }

        
        public void Configure(IApplicationBuilder app)
        {
            app.UseDeveloperExceptionPage();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers(); 
            });
        }
    }
}
