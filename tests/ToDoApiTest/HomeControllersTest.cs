﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using EFDataAccessLibrary.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using ToDoNormalWebApiDB;
using ToDoNormalWebApiDB.Controllers;

namespace ToDoApiTest
{

    public class HomeControllersTest
    {
        // экз контроллера в методах
        //
        //убрать поле, всучить в методы!!!!

       
        // могу пробрасывать в методе экспрешн (если сильно хочется), обязательно глянуть
        // пирамида тестирования 

        [Test] // мокаю сам репозиторий
        public async void Get_GivenId_ReturnsTaskWithSameId() // для контроллера
        {
            int id = 1;
            ToDoTask toDoTask = new ToDoTask();
            toDoTask.Id = id;

            Mock<Task<IRepository<ToDoTask>>> mock = new Mock<Task<IRepository<ToDoTask>>> (MockBehavior.Strict);
            mock.Setup(repo  => repo.Result.GetTask(id)).Returns(Task.FromResult(toDoTask));
            HomeController controller = new HomeController(mock.Object.Result);

            // Act
            ObjectResult result = controller.Get(id) as ObjectResult;
            ToDoTask resultObject = result.Value as ToDoTask;

            // Assert
            Assert.AreEqual(resultObject.Id, id);
            mock.Verify(repo => repo.Result.GetTask(id), Times.Once);
           
        }

        [Test] // мокаю сам репозиторий
        public async void Create_GivenTodoTask_ReturnsIActionResult()
        {
            ToDoTask toDoTask = new ToDoTask();
            //mock.Setup(repo => repo.Create(toDoTask));
            Mock<IRepository<ToDoTask>> mock = new Mock<IRepository<ToDoTask>>(MockBehavior.Strict);
            mock.Setup(repo => repo.Create(toDoTask));
            mock.Setup(repo => repo.Save());

            HomeController controller = new HomeController(mock.Object);
            IActionResult result = controller.Create(toDoTask);
            ObjectResult objectResult = result as ObjectResult;
           
            Assert.AreEqual(objectResult.Value, toDoTask); //хорошо
            mock.Verify(repo => repo.Create(toDoTask), Times.Once);
            mock.Verify(repo => repo.Save(), Times.Once); // такая проверка везде
        }

        [Test]
        public async void Update_ActionResult_ReturnsActionResult()
        {
            ToDoTask toDoTask = new ToDoTask();

            Mock<IRepository<ToDoTask>> mock = new Mock<IRepository<ToDoTask>>(MockBehavior.Strict);
            mock.Setup(repo => repo.Update(toDoTask));
            mock.Setup(repo => repo.Save());

            HomeController controller = new HomeController(mock.Object);
            IActionResult result = controller.Update(toDoTask);
            ObjectResult objectResult = result as ObjectResult;

            Assert.AreSame(toDoTask, objectResult.Value); //
            mock.Verify(repo => repo.Update(toDoTask), Times.Once);
            mock.Verify(repo => repo.Save(), Times.Once); 
            // наследование обжектрезалтов и тд
        }



        [Test]
        public async void Delete_GivenId_ActionResult()
        {
            int id = 1;
            ToDoTask toDoTask = new ToDoTask();
            toDoTask.Id = id;

            Mock<IRepository<ToDoTask>> mock = new Mock<IRepository<ToDoTask>>(MockBehavior.Strict);
            mock.Setup(repo => repo.Delete(id));
            mock.Setup(repo => repo.GetTask(id)).Returns(Task.FromResult(toDoTask));
            mock.Setup(repo => repo.Save());
            HomeController controller = new HomeController(mock.Object);

            
            StatusCodeResult result = controller.Delete(id) as StatusCodeResult;
            

            Assert.AreEqual(result.StatusCode, StatusCodes.Status200OK);
            mock.Verify(repo => repo.Delete(id), Times.Once);
            mock.Verify(repo => repo.GetTask(id), Times.Once);
            mock.Verify(repo => repo.Save(), Times.Once);
        }


    }
}
