﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using EFDataAccessLibrary.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using ToDoNormalWebApiDB;
using ToDoNormalWebApiDB.Controllers;
using Microsoft.EntityFrameworkCore;
using EFDataAccessLibrary.DataAccess;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoApiTest
{
    public class SQLTaskRepositoryTest
    {
        [Test]
        public async void GetTaskList_ReturnsSavedDbItem()
        {
            DbContextOptions<Context> options = new DbContextOptionsBuilder<Context>()
            .UseInMemoryDatabase(databaseName: nameof(GetTaskList_ReturnsSavedDbItem))
            .Options;

            Context context = new Context(options);
            int testId = 1;
            string testTitle = "Lincoln";
            string testDescription = "23 Tsawassen Blvd.";

            ToDoTask toDoTask = new ToDoTask
            {
                Id = testId,
                Title = testTitle,
                Description = testDescription
            };

            context.Tasks.Add(toDoTask);
            context.SaveChanges();

            SQLTaskRepository sQLTaskRepository = new SQLTaskRepository(context);


            // act
            IEnumerable<ToDoTask> result = sQLTaskRepository.GetTaskList().Result.ToList();

            ToDoTask resulTask = result.First();
            Assert.AreEqual(resulTask.Id, testId);
            Assert.AreEqual(resulTask.Title, testTitle);
            Assert.AreEqual(resulTask.Description, testDescription);


        }

        [Test]
        public async void GetTask_GivenID_ReturnsIEnumerable()
        {
            DbContextOptions<Context> options = new DbContextOptionsBuilder<Context>()
            .UseInMemoryDatabase(databaseName: nameof(GetTask_GivenID_ReturnsIEnumerable))
            .Options;

            Context context = new Context(options);
            int testId = 1;
            string testTitle = "Lincoln";
            string testDescription = "23 Tsawassen Blvd.";

            ToDoTask toDoTask = new ToDoTask
            {
                Id = testId,
                Title = testTitle,
                Description = testDescription
            };

            context.Tasks.Add(toDoTask);
            context.SaveChanges();

            SQLTaskRepository sQLTaskRepository = new SQLTaskRepository(context);
            ToDoTask result = sQLTaskRepository.GetTask(testId).Result;

            Assert.AreEqual(result.Id, testId);
            Assert.AreEqual(result.Title, testTitle);
            Assert.AreEqual(result.Description, testDescription);
        }

        [Test]
        public async void Create_GivenID_ReturnsIEnumerable()
        {
            DbContextOptions<Context> options = new DbContextOptionsBuilder<Context>()
            .UseInMemoryDatabase(databaseName: nameof(Create_GivenID_ReturnsIEnumerable))
            .Options;


            Context context = new Context(options);


            int testId = 1;
            string testTitle = "Lincoln";
            string testDescription = "23 Tsawassen Blvd.";

            ToDoTask toDoTask = new ToDoTask
            {
                Id = testId,
                Title = testTitle,
                Description = testDescription
            };

            context.SaveChanges();

            SQLTaskRepository sQLTaskRepository = new SQLTaskRepository(context);
            sQLTaskRepository.Create(toDoTask);
            sQLTaskRepository.Save();

            context.Tasks.Single();
            context.SaveChanges();

            Assert.IsNotEmpty(context.Tasks);
        }

        [Test]
        public async void Update_GivenID_CreatesTaskInDb()
        {
            DbContextOptions<Context> options = new DbContextOptionsBuilder<Context>()
            .UseInMemoryDatabase(databaseName: nameof(Update_GivenID_CreatesTaskInDb))
            .Options;

            Context context = new Context(options);
            int testId = 1;
            string testTitle = "Lincoln";
            string testDescription = "23 Tsawassen Blvd.";

            ToDoTask toDoTask = new ToDoTask
            {
                Id = testId,
                Title = testTitle,
                Description = testDescription
            };

            context.Tasks.Add(toDoTask);
            context.SaveChanges();

            toDoTask.Title = "new title";
            string newTitle = toDoTask.Title;

            toDoTask.Description = "new description";
            string newDescription = toDoTask.Description;


            SQLTaskRepository sQLTaskRepository = new SQLTaskRepository(context);
            sQLTaskRepository.Update(toDoTask);
            context.SaveChanges();

            ToDoTask single = context.Tasks.Single();


            Assert.AreEqual(single.Id, testId);
            Assert.AreEqual(single.Title, newTitle);
            Assert.AreNotEqual(single.Description, testDescription);

        }

        [Test]
        public async void Delete_GivenID_ReturnsDeletedTaskInDb()
        {
          DbContextOptions<Context> options = new DbContextOptionsBuilder<Context>()
          .UseInMemoryDatabase(databaseName: nameof(Delete_GivenID_ReturnsDeletedTaskInDb))
          .Options;

            Context context = new Context(options);
            int testId = 1;
            string testTitle = "Lincoln";
            string testDescription = "23 Tsawassen Blvd.";

            ToDoTask toDoTask = new ToDoTask
            {
                Id = testId,
                Title = testTitle,
                Description = testDescription
            };

            context.Tasks.Add(toDoTask);
            context.SaveChanges();


            SQLTaskRepository sQLTaskRepository = new SQLTaskRepository(context);
            sQLTaskRepository.Delete(testId);
            context.SaveChanges();

            Assert.IsEmpty(context.Tasks);

        }

        [Test]
        public async void Save_ReturnsSavedTaskInDb()
        {

           DbContextOptions<Context> options = new DbContextOptionsBuilder<Context>()
          .UseInMemoryDatabase(databaseName: nameof(Save_ReturnsSavedTaskInDb))
          .Options;

            Context context = new Context(options);
            int testId = 1;
            string testTitle = "Lincoln";
            string testDescription = "23 Tsawassen Blvd.";

            ToDoTask toDoTask = new ToDoTask
            {
                Id = testId,
                Title = testTitle,
                Description = testDescription
            };

            context.Tasks.Add(toDoTask);
            context.SaveChanges();

            SQLTaskRepository sQLTaskRepository = new SQLTaskRepository(context);
            sQLTaskRepository.Save();

            Assert.AreEqual(context.Tasks.Single(), toDoTask);
        }

    }
}
